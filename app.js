const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const animalRoutes = require('./routes/animalRoutes');

const animalPlanetsAPI = express();

animalPlanetsAPI.use(bodyParser.json());

animalPlanetsAPI.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
  res.setHeader("Access-Control-Allow-Headers", "Content/Type, Authorization");
  next();
});

animalPlanetsAPI.use('/animals', animalRoutes);

mongoose.connect(
    'mongodb+srv://Carlos:goonlive92@cluster0-woub5.mongodb.net/animalplanets?retryWrites=true&w=majority',
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false
    }
  )
  .then(() => {
    const server = animalPlanetsAPI.listen(8080);
    const io = require('./socket').init(server);
    io.on('connection', () => {
      console.log('Client connected');
    });
  })
  .catch(err => console.log(err));