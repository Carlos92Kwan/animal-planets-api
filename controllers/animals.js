const { validationResult } = require('express-validator/check');

const Animals = require('../models/animals');

const io = require('../socket');

exports.getAllAnimals = async (req, res, next) => {

  const allAnimals = await Animals.find();

  res.status(200).json({
    message: 'Fetched animals successfully.',
    allAnimals: allAnimals
  });

};

exports.searchAnimals = async (req, res, next) => {
  const animalQuery = req.params.animal;
  const searchResults = await Animals
    .find({ $text: { $search: animalQuery } },
      { score: { $meta: "textScore" } })
    .sort({ score: { $meta: "textScore" } });

  res.status(200).json({
    message: 'Search results success!',
    searchResults: searchResults
  });

};

exports.addAnimal = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({
      message: 'Validation failed, entered data is incorrect.',
      errors: errors.array()
    });
  }

  const name = req.body.name;
  const locations = req.body.locations;

  console.log(locations);

  const newAnimal = new Animals(
    {
      name: name,
      locations: []
    }
  );

  newAnimal.save()
    .then(() => {
      Animals.findOneAndUpdate(
        { name: name },
        { $push: { locations: { $each: locations } } },
        { new: true })
        .then(result => {
          io.getIO().emit('newAnimal', {
            newAnimal: newAnimal
          });
          res.status(201).json({
            message: 'Animal added successfully!',
            animal: result
          });
        })
        .catch(err => {
          console.log(err);
        });
    })
    .catch(err => {
      console.log(err);
    });
};
