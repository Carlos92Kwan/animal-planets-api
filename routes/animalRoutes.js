const express = require('express');
const { body } = require('express-validator/check');

const animalController = require('../controllers/animals');

const router = express.Router();

// GET /animals/all
router.get('/all', animalController.getAllAnimals);

// GET /animals/search/deer
router.get('/search/:animal', animalController.searchAnimals);

// POST /animals/add
router.post('/add', [body('name').trim(), body('location').trim()],
animalController.addAnimal);

module.exports = router;